package com.upnest.main;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class Buy_Page {
    public static By byCityAndStateOrZip = By.id("buysellHeroFormLocation");
    public static By byHitCompareAgentsButton = By.cssSelector("#buysellHeroForm > div.inputs > input.next-button");
    public static By byClickbuyOption = By.xpath("/html/body/div[4]/div[2]/div/div[3]/div[2]");
    //public static By byChoosingSellQuicklyOption = By.cssSelector("#agent-preferences-div > div:nth-child(1)");
    public static By byChoosingPrice = By.cssSelector("#price-ranges-div > div:nth-child(1)");
    public static By byChoosingHomeType = By.cssSelector("#home-type-div > div:nth-child(1)");
    public static By byChoosingProcess = By.cssSelector("#buyer-stage-div > div:nth-child(1)");
    public static By byChoosingLender = By.cssSelector("#buyer-stage-div > div:nth-child(1)");
   // public static By byChoosingPrice = By.xpath("/html/body/div[4]/div[2]/div/div[2]/div/div[1]");
   // public static By byAddAddress = By.id("address");
    public static By byAddName = By.id("name");
    public static By byAddEmail = By.id("email");
    public static By byAddPh = By.id("phone");
    public static By byHitContinueButton = By.className("continue-button");
    //public static By byHitNoChatBot = By.cssSelector("#reply-buttons-container > div:nth-child(2)");
    public static By byHitOkChatBot = By.cssSelector("#reply-buttons-container > div:nth-child(2)");
   // public static By byHitAlreadyListedChatBot = By.cssSelector("#reply-buttons-container > div:nth-child(1)");
    //public static By byHitYesChatBot = By.cssSelector("#reply-buttons-container > div:nth-child(2)");
    public static By byHitSkipChatBot = By.cssSelector("#reply-buttons-container > div.skip-dt");
   // public static By byConfirmEmail = By.id("send-email-button");
   // public static By byMyProposalTitle = By.cssSelector("#sellBuyWrapp > h2");
    //public static By byConfirmationText = By.className("next-heading");

    WebDriver driver;

    public Buy_Page(WebDriver driver) {
        this.driver=driver;
    }
    public Buy_Page(){

    }

    public void enterLocation (String location) throws InterruptedException {
        driver.findElement(byCityAndStateOrZip).clear();
        Thread.sleep(3000);
        driver.findElement(byCityAndStateOrZip).sendKeys(location);
    }

    public void clickCompareAgentsButton(){
        driver.findElement(byHitCompareAgentsButton).click();
    }

    public void selectbuyOption (){
        driver.findElement(byClickbuyOption).click();
    }

    /*public void sellQuicklyOption (){
        driver.findElement(byChoosingSellQuicklyOption).click();
    }*/

    public void selectPrice () throws InterruptedException {

        driver.findElement(byChoosingPrice).click();
        Thread.sleep(5000);
    }


    public void selectHomeType () throws InterruptedException {
        driver.findElement(byChoosingHomeType).click();
        Thread.sleep(3000);
    }
    public  void selectProcess () throws InterruptedException{
        Thread.sleep(3000);
        driver.findElement(byChoosingProcess).click();

    }


    public void selectLender () throws InterruptedException{
        Thread.sleep(1000);
        driver.findElement(byChoosingLender).click();

    }

    public void addName (String name){
        driver.findElement(byAddName).sendKeys(name);
    }
   /* public void addAddress (String address){
        driver.findElement(byAddAddress).sendKeys(address);
    }*/

    public void addEmail (String email){
        driver.findElement(byAddEmail).sendKeys(email);
    }

    public void addPh (String ph){
        driver.findElement(byAddPh).sendKeys(ph);
    }

    public void continueButton (){
        driver.findElement(byHitContinueButton).click();
    }

    public void okChatBot (){
        driver.findElement(byHitOkChatBot).click();
    }
/*
    public void selectAnyOptionChatBot (){
        driver.findElement(byHitAlreadyListedChatBot).click();
    }
*/
  /*  public void yesChatBot (){
        driver.findElement(byHitYesChatBot).click();
    }*/

    public void skipChatBot (){
        driver.findElement(byHitSkipChatBot).click();
    }

    //public void getConfirmEmail () {driver.findElement(byConfirmEmail).click();}

    //public String getProposalTitle () {return driver.findElement(byMyProposalTitle).getText();}

    //public String getConfirmationText () {return driver.findElement(byConfirmationText).getText();}




}
