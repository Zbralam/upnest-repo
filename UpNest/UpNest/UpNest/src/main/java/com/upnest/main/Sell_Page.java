package com.upnest.main;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Sell_Page {

    public static By byCityAndStateOrZip = By.id("buysellHeroFormLocation");
    public static By byHitCompareAgentsButton = By.cssSelector("#buysellHeroForm .next-button");
    public static By byClickSellOption = By.className("sell-button");
    public static By byChoosingSellQuicklyOption = By.cssSelector("#agent-preferences-div > :nth-child(1)");
    public static By byChoosingHomeType = By.cssSelector("#home-type-div > :nth-child(1)");
   //public static By byChoosingPrice = By.cssSelector("#price-ranges-div > div:nth-child(1)");
    public static By byChoosingPrice = By.cssSelector("#price-ranges-div >:nth-child(1)");
    public static By byAddAddress = By.id("address");
    public static By byAddName = By.id("name");
    public static By byAddEmail = By.id("email");
    public static By byAddPh = By.id("phone");
    public static By  byHitContinueButton = By.cssSelector(".continue-button");
    public static By byHitNoChatBot = By.cssSelector("#reply-buttons-container > div:nth-child(2)");
    public static By byHitAlreadyListedChatBot = By.cssSelector("#reply-buttons-container > div:nth-child(1)");
    public static By byHitYesChatBot = By.cssSelector("#reply-buttons-container > div:nth-child(2)");
    public static By byHitSkipChatBot = By.cssSelector("#reply-buttons-container > div.skip-dt");
    public static By byConfirmEmail = By.id("send-email-button");
    public static By byMyProposalTitle = By.cssSelector("#sellBuyWrapp > h2");
    public static By byConfirmationText = By.className("next-heading");

    WebDriver driver;

    public Sell_Page(WebDriver driver) {
        this.driver=driver;
    }
    public Sell_Page(){

    }

    public void enterLocation (String location) throws InterruptedException {
        driver.findElement(byCityAndStateOrZip).clear();
        Thread.sleep(3000);
        driver.findElement(byCityAndStateOrZip).sendKeys(location);
    }

    public void clickCompareAgentsButton(){
        driver.findElement(byHitCompareAgentsButton).click();
    }

    public void selectSellOption (){
        driver.findElement(byClickSellOption).click();
    }

    public void sellQuicklyOption (){
        driver.findElement(byChoosingSellQuicklyOption).click();
    }

    public void selectHomeType () throws InterruptedException {
        driver.findElement(byChoosingHomeType).click();
        Thread.sleep(3000);
    }

    public void selectPrice () throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(byChoosingPrice).click();
    }

    public void addAddress (String address){
        driver.findElement(byAddAddress).sendKeys(address);
    }

    public void addName (String name){
        driver.findElement(byAddName).sendKeys(name);
    }

    public void addEmail (String email){
        driver.findElement(byAddEmail).sendKeys(email);
    }

    public void addPh (String ph){
        driver.findElement(byAddPh).sendKeys(ph);
    }

    public void continueButton (){
        driver.findElement(byHitContinueButton).click();
    }

    public void noChatBot (){
        driver.findElement(byHitNoChatBot).click();
    }

    public void selectAnyOptionChatBot (){
        driver.findElement(byHitAlreadyListedChatBot).click();
    }

    public void yesChatBot (){
        driver.findElement(byHitYesChatBot).click();
    }

    public void skipChatBot (){
        driver.findElement(byHitSkipChatBot).click();
    }

    public void getConfirmEmail () {driver.findElement(byConfirmEmail).click();}

    public String getProposalTitle () {return driver.findElement(byMyProposalTitle).getText();}

    public String getConfirmationText () {return driver.findElement(byConfirmationText).getText();}
}
