package com.upnest.main;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Zubair on 12/28/2019.
 *
 **/

public class Sell {
    ChromeDriver driver;


    @Before
    public void launchUpNestApp() throws InterruptedException, AWTException {

        String localDir = System.getProperty("user.dir");
        File file = new File(localDir + "\\driver\\chromedriver.exe");

        System.setProperty("webdriver.chrome.driver", File.separator + file);

        driver = new ChromeDriver();
        driver.get("https://beta.upnest.com/");
        driver.manage().window().maximize();

    }



    @Test
    public void testCaseForSell() throws InterruptedException, IOException {
        Thread.sleep(3000);
        Sell_Page sellPage = new Sell_Page(driver);
        Thread.sleep(3000);
        sellPage.enterLocation("Palo Alto, CA");
        sellPage.clickCompareAgentsButton();
        Thread.sleep(3000);
        sellPage.selectSellOption();
        Thread.sleep(3000);
        sellPage.sellQuicklyOption();
        Thread.sleep(3000);
        sellPage.selectHomeType();
        Thread.sleep(3000);
        sellPage.selectPrice();
        Thread.sleep(3000);
        sellPage.addAddress("1255 Pear Avenue, Mountain View, CA, USA");
        sellPage.addName("Zubair Alam");
        sellPage.continueButton();
        Thread.sleep(5000);
        sellPage.addEmail("zubair_alam" + getRandomNumber() +"@youp.com");
        sellPage.continueButton();
        Thread.sleep(5000);
        sellPage.addPh("3453453452");
        Thread.sleep(4000);
        sellPage.continueButton();
        Thread.sleep(4000);
        sellPage.noChatBot();
        Thread.sleep(3000);
        sellPage.selectAnyOptionChatBot();
        Thread.sleep(3000);
        sellPage.yesChatBot();
        Thread.sleep(3000);
        sellPage.skipChatBot();
        Thread.sleep(3000);
        Thread.sleep(5000);

    }


    public static String getRandomNumber() throws IOException {
        long n = 1000 + new Random().nextInt(9000);
        String rand = "3" + n;
        System.out.println("Random Phone Number: " + rand);
        return rand;
    }

    @After
    public void closeApp(){
        driver.close();

    }

}

