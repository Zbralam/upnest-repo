package com.upnest.main;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import static com.upnest.main.Sell.getRandomNumber;

/**
 * Created by Zubair on 12/28/2019.
 *
 **/

public class Buy {
    ChromeDriver driver;


    @Before
    public void launchUpNestApp() throws InterruptedException, AWTException {

        String localDir = System.getProperty("user.dir");
        File file = new File(localDir + "\\driver\\chromedriver.exe");

        System.setProperty("webdriver.chrome.driver", File.separator + file);

        driver = new ChromeDriver();
        driver.get("https://beta.upnest.com/");
        driver.manage().window().maximize();

    }


    @Test
    public void testCaseForBuy() throws InterruptedException, IOException {
        Thread.sleep(3000);
   //     driver.get("https://beta.upnest.com/");
        Buy_Page buyPage = new Buy_Page(driver);
        Thread.sleep(4000);
        buyPage.enterLocation("Palo Alto, CA");
        buyPage.clickCompareAgentsButton();
        Thread.sleep(5000);
        buyPage.selectbuyOption();
        Thread.sleep(5000);
        buyPage.selectPrice();
        Thread.sleep(5000);
        buyPage.selectHomeType();
        Thread.sleep(5000);
        buyPage.selectProcess();
        Thread.sleep(5000);
        buyPage.selectLender();
        Thread.sleep(5000);
        buyPage.addName("Zubair Alam");
        Thread.sleep(5000);
        buyPage.addEmail("zubair_alam" + getRandomNumber() +"@youp.com");
        Thread.sleep(7000);
        buyPage.continueButton();
        Thread.sleep(4000);
        buyPage.addPh("3453453452");
        Thread.sleep(4000);
        buyPage.continueButton();
        Thread.sleep(4000);
        buyPage.okChatBot();
        Thread.sleep(2000);
        buyPage.skipChatBot();
        Thread.sleep(2000);
        buyPage.skipChatBot();
        Thread.sleep(2000);
    }

    @After
    public void closeApp(){
        driver.close();

    }

}

